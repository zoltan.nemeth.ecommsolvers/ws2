<?php
namespace Ws\Controllers;

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Ws\Base\Controller;

class HomeController extends Controller
{
	
	const TITLE = 'Home';
	
	public function __construct()
	{
		parent::__construct();
	}
	
	public function index(Request $request, Response $response, array $args)
	{
		$response->getBody()->write('Webshop 2 application');
		
		return $response;
	}
	
}