<?php
declare(strict_types=1);

use DI\ContainerBuilder;
use Monolog\Logger;

return function (ContainerBuilder $containerBuilder)
{
	// Global Settings Object
	$containerBuilder->addDefinitions(
		[
			'settings' => [
				'displayErrorDetails' => true,
				'logger'              => [
					'name'  => 'slim-app',
					'path'  => __DIR__ . '/../logs/app.log',
					'level' => Logger::DEBUG,
				],
			],
		]
	);
};