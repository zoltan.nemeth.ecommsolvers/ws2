<?php
declare(strict_types=1);

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Slim\App;
use Slim\Interfaces\RouteCollectorProxyInterface as Group;
use Ws\Controllers\HomeController;

return function (App $app)
{
	$container = $app->getContainer();
	$app->get('/', HomeController::class . ':index');
};
